This repository is a target for the output of a custom pipeline that runs
in `alacrity-rs` repository in Sourcehut.

Please, DO NOT EDIT this repository in any way.
